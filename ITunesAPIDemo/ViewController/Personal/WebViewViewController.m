//
//  WebViewViewController.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/27.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "WebViewViewController.h"

@interface WebViewViewController ()

@end

@implementation WebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	if (self.urlString) {
		[self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
	}
}

- (IBAction)closeButtonDidPress:(UIButton *)sender {
	[self dismissViewControllerAnimated:true completion:nil];
}



@end
