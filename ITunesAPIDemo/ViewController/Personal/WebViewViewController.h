//
//  WebViewViewController.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/27.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WebViewViewController : UIViewController

@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (strong, nonatomic) NSString *urlString;

@end

NS_ASSUME_NONNULL_END
