//
//  ThemeColorViewController.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/27.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "ThemeColorViewController.h"
#import "ThemeColor.h"

@interface ThemeColorViewController () <UITableViewDelegate, UITableViewDataSource>

// 主題顏色標題
@property (strong, nonatomic) NSArray *themeColorTitles;

// 選中的indexPath
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation ThemeColorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	[self setupViews];
}


#pragma mark - Settings

- (void)setupViews {
	[self.navigationItem setTitle:@"主題顏色"];
	self.themeColorTitles = @[@"深色主題", @"淺色主題"];
	[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
	[self checkTheme];
}

/// Pre-select the indexPath according to the saved themeType
- (void)checkTheme {
	switch ([ThemeColor sharedInstance].currentTheme.themeType) {
		case dark:
			self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
			break;
		case light:
			self.selectedIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
			break;
		default:
			break;
	}
}

- (void)setupThemeColors {
	UIColor *themeColor = [ThemeColor sharedInstance].currentTheme.color;
	[self.navigationController.navigationBar setBarTintColor:themeColor];
	[self.tabBarController.tabBar setTintColor:themeColor];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.themeColorTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class]) forIndexPath:indexPath];
	[cell.textLabel setText:self.themeColorTitles[indexPath.row]];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	[cell setAccessoryType:UITableViewCellAccessoryNone];
	if (self.selectedIndexPath == indexPath) {
		[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
	}
	return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	self.selectedIndexPath = indexPath;
	[self.tableView reloadData];
	
	switch (indexPath.row) {
		case 0:
			[[ThemeColor sharedInstance] saveThemeType:dark];
			break;
		case 1:
			[[ThemeColor sharedInstance] saveThemeType:light];
		default:
			break;
	}
	
	[self setupThemeColors];
}

@end
