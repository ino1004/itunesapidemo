//
//  PersonalViewController.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "PersonalViewController.h"
#import "PersonalCell.h"
#import "WebViewViewController.h"
#import "ThemeColor.h"

@interface PersonalViewController () <UITableViewDelegate, UITableViewDataSource>

// 標題
@property (strong, nonatomic) NSArray *titles;

// FooterView's height
@property (nonatomic) CGFloat footerViewHeight;

@end

@implementation PersonalViewController

static NSString *itunesSupportURLString = @"https://support.apple.com/itunes";

- (void)viewDidLoad {
    [super viewDidLoad];
	[self setupViews];
	[self setupThemeColor];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.tableView reloadData];
}


#pragma mark - Settings

- (void)setupViews {
	self.titles = @[@"主題顏色", @"收藏項目"];
	[self.navigationItem setTitle:@"個人資料"];
	[self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PersonalCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PersonalCell class])];
	self.footerViewHeight = 40;
}

- (void)setupThemeColor {
	UIColor *themeColor = [ThemeColor sharedInstance].currentTheme.color;
	[self.navigationController.navigationBar setBarTintColor:themeColor];
}


#pragma mark - Action

- (void)footerViewButtonDidPress:(UIButton *)sender {
	// Modal present to WebViewVC
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"WebView" bundle:nil];
	WebViewViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"WebViewViewController"];
	vc.urlString = itunesSupportURLString;
	[self presentViewController:vc animated:true completion:nil];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.titles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	PersonalCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PersonalCell class]) forIndexPath:indexPath];
	
	[cell bindingWithTitle:self.titles[indexPath.section] theme:[ThemeColor sharedInstance].currentTheme atIndexPath:(NSIndexPath *)indexPath];
	return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSString *identifier = @"";
	switch (indexPath.section) {
		case 0:
			identifier = @"ThemeColorViewController";
			break;
		case 1:
			identifier = @"FavoriteViewController";
			break;
		default:
			break;
	}
	
	if (![identifier isEqualToString:@""]) {
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PersonalDetail" bundle:nil];
		UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:identifier];
		[self.navigationController pushViewController:vc animated:true];
	}
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
	if (section == self.titles.count - 1) {
		// Footer view
		UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [tableView bounds].size.width, self.footerViewHeight)];
		[footerView setBackgroundColor:[UIColor clearColor]];
		
		// Footer view's button setting
		UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
		UIImage *image = [[UIImage imageNamed:@"about"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
		[button setImage:image forState:UIControlStateNormal];
		[button setTintColor:[UIColor darkGrayColor]];
		[button setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
		[button setTitle:@"關於Apple iTunes" forState:UIControlStateNormal];
		[button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
		[button addTarget:self action:@selector(footerViewButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
		[footerView addSubview:button];
		button.translatesAutoresizingMaskIntoConstraints = false;
		[[button.topAnchor constraintEqualToAnchor:footerView.topAnchor] setActive:true];
		[[button.trailingAnchor constraintEqualToAnchor:footerView.trailingAnchor constant:-5] setActive:true];
		[[button.bottomAnchor constraintEqualToAnchor:footerView.bottomAnchor] setActive:true];
		
		return footerView;
	}
	
	return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
	if (section == self.titles.count - 1) {
		return self.footerViewHeight;
	}
	
	return 0.01;
}

@end
