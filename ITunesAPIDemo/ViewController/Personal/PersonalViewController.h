//
//  PersonalViewController.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PersonalViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
