//
//  FavoriteViewController.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/27.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "FavoriteViewController.h"
#import "FavoritePageCell.h"
#import "FavoriteManager.h"
#import "TrackCell.h"

@interface FavoriteViewController () <UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout>

// 目前的index
@property (nonatomic) int currentPageIndex;

// 記錄viewWillDisappear前最後的index
@property (nonatomic) int lastPageIndex;

@end

@implementation FavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	[self setupViews];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[self setupContentOffset];
	[self updateCell];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	self.lastPageIndex = self.currentPageIndex;
}


#pragma mark - Settings

- (void)setupViews {
	self.currentPageIndex = 0;
	self.lastPageIndex = 0;
	[self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([FavoritePageCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([FavoritePageCell class])];
}

- (void)setupContentOffset {
	[self.collectionView setContentOffset:CGPointMake(0, 0)];
}

- (void)updateCell {
	// Keeping the last page index and reload it
	[self.collectionView reloadData];
	for (FavoritePageCell *cell in self.collectionView.visibleCells) {
		[cell.tableView reloadData];
	}
	
	[self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.lastPageIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:false];
}


#pragma mark - IBAction

- (IBAction)segmentedControlValueDidChange:(UISegmentedControl *)sender {
	self.currentPageIndex = (int)sender.selectedSegmentIndex;
	[self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.currentPageIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:true];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
	return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	FavoritePageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([FavoritePageCell class]) forIndexPath:indexPath];
	TrackKind kind = movie;
	if (indexPath.item == 1) {
		kind = music;
	}
	[cell bindingWithKind:kind];
	return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	return CGSizeMake(self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
	return 0;
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	self.currentPageIndex = self.collectionView.contentOffset.x / self.collectionView.frame.size.width;
	[self.segmentedControl setSelectedSegmentIndex:self.currentPageIndex];
}


#pragma mark - FavoritePageCellDelegate

- (void)favoritePageCell:(FavoritePageCell *)favoritePageCell favoriteButtonDidPress:(UIButton *)button {
	[favoritePageCell.tableView reloadData];
}

@end
