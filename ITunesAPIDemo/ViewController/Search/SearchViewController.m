//
//  ViewController.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "SearchViewController.h"
#import "TrackCell.h"
#import "DataManager.h"
#import "TrackList.h"
#import "FavoriteManager.h"
#import "ThemeColor.h"
#import "Networking.h"

@interface SearchViewController () <UITableViewDelegate, UITableViewDataSource, TrackCellDelegate, UISearchBarDelegate>

// All tracks
@property (strong, nonatomic) NSArray<TrackList *> *allTrackList;

// HeaderView's height
@property (nonatomic) CGFloat headerViewHeight;

// Expanded indexPaths
@property (strong, nonatomic) NSMutableArray<NSIndexPath *> *expandedIndexPaths;

// Empty label
@property (strong, nonatomic) UILabel *emptyLabel;

@end

@implementation SearchViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	[self setupViews];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.tableView reloadData];
	[self setupThemeColors];
}


#pragma mark - Settings

- (void)setupViews {
	self.headerViewHeight = 50;
	self.expandedIndexPaths = [NSMutableArray new];
	
	// Register cell
	[self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TrackCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([TrackCell class])];
	
	[self.tableView setHidden:true];
	self.allTrackList = [NSArray new];
	
	[self setupEmptyLabel];
}

- (void)setupEmptyLabel {
	self.emptyLabel = [UILabel new];
	[self.emptyLabel setText:@"沒有資料"];
	[self.view addSubview:self.emptyLabel];
	[self.emptyLabel setTranslatesAutoresizingMaskIntoConstraints:false];
	[[self.emptyLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor] setActive:true];
	[[self.emptyLabel.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor] setActive:true];
}

- (void)setupThemeColors {
	UIColor *themeColor = [ThemeColor sharedInstance].currentTheme.color;
	[self.searchBar setBarTintColor:themeColor];
	[self.tabBarController.tabBar setTintColor:themeColor];
}


#pragma mark - Data

- (void)getDataWithKeyword:(NSString *)keyword {
	[[DataManager sharedInstance] getDataWithKeyword:keyword successHandler:^(NSArray * _Nonnull trackList) {
		self.allTrackList = trackList;
		dispatch_async(dispatch_get_main_queue(), ^{
			if (trackList.count == 0) {
				[self.emptyLabel setHidden:false];
				[self.tableView setHidden:true];
			} else {
				[self.emptyLabel setHidden:true];
				[self.tableView setHidden:false];
				[self.tableView reloadData];
			}
		});
	} failureHandler:^(NSError * _Nonnull error) {
		// Error handling
		
	}];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.allTrackList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSArray *list = self.allTrackList[section].tracks;
	return list == nil ? 0 : list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	TrackCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TrackCell class]) forIndexPath:indexPath];
	TrackList *trackList = self.allTrackList[indexPath.section];
	Track *track = trackList.tracks[indexPath.row];
	
	cell.delegate = self;
	[cell bindingWithTrack:track kind:trackList.kind];
	[cell checkExpandedIndexPath:indexPath inIndexPaths:self.expandedIndexPaths kind:trackList.kind];
	return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	Track *track = self.allTrackList[indexPath.section].tracks[indexPath.row];
	
	NSURL *url = [NSURL URLWithString:track.trackViewURLString];
	[[DataManager sharedInstance] openURL:url];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	// Setup the headerView
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [tableView bounds].size.width, self.headerViewHeight)];
	[headerView setBackgroundColor:[UIColor whiteColor]];
	
	// Setup the title label
	UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, [headerView bounds].size.width - 20, [headerView bounds].size.height)];
	[titleLabel setBackgroundColor:[UIColor whiteColor]];
	
	// Title of the header
	NSString *title = @"";
	switch (self.allTrackList[section].kind) {
		case movie:
			title = @"電影";
			break;
		case music:
			title = @"音樂";
			break;
		default:
			break;
	}
	[titleLabel setText:title];
	[headerView addSubview:titleLabel];
	
	return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return self.headerViewHeight;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewAutomaticDimension;
}


#pragma mark - TrackCellDelegate

/// 收藏button
- (void)trackCell:(TrackCell *)trackCell favoriteButtonDidPress:(UIButton *)button {
	NSIndexPath *indexPath = [self.tableView indexPathForCell:trackCell];
	TrackList *trackList = self.allTrackList[indexPath.section];
	Track *track = trackList.tracks[indexPath.row];
	
	BOOL isFavorited = [[FavoriteManager sharedInstance] isFavoritedWithTrack:track kind:trackList.kind];
	[trackCell setFavorite:isFavorited];
}

/// ReadMore button
- (void)trackCell:(TrackCell *)trackCell readMoreButtonDidPress:(UIButton *)button {
	NSIndexPath *indexPath = [self.tableView indexPathForCell:trackCell];
	[self.expandedIndexPaths addObject:indexPath];
	[self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}


#pragma mark - SearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
	
	if (![searchBar.text isEqualToString:@""]) {
		[self.expandedIndexPaths removeAllObjects];
		[self getDataWithKeyword:searchBar.text];
	}
}


@end
