//
//  Networking.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "Networking.h"
#import <AFNetworking.h>

static NSString *urlString = @"https://itunes.apple.com/search?";

@implementation Networking

- (void)getURLWithKeyword:(NSString *)keyword success:(void (^)(id responseObject))successHandler failure:(void (^)(NSError *error))failureHandler {
	
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	
	NSDictionary *countryParam = @{@"term": keyword,
								   @"country": @"TW"};
	
	[manager GET:urlString parameters:countryParam progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
		successHandler(responseObject);
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
		failureHandler(error);
	}];
}

@end
