//
//  Networking.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Networking : NSObject

- (void)getURLWithKeyword:(NSString *)keyword success:(void (^)(id responseObject))successHandler failure:(void (^)(NSError *error))failureHandler;

@end

NS_ASSUME_NONNULL_END
