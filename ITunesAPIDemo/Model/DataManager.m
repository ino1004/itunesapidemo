//
//  DataManager.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "DataManager.h"
#import "Networking.h"
#import "TrackList.h"

@implementation DataManager

+ (instancetype)sharedInstance {
	static DataManager *dataManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		dataManager = [[DataManager alloc] init];
	});
	return dataManager;
}

/// Get Data from API
- (void)getDataWithKeyword:(NSString *)keyword successHandler:(void (^)(NSArray * trackList))successHandler failureHandler:(void (^)(NSError *error))failureHandler {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		Networking *networking = [Networking new];
		[networking getURLWithKeyword:keyword success:^(id  _Nonnull responseObject) {
			// Get the results
			NSArray *array = responseObject[@"results"];
			
			// Initialize the tracks
			NSArray *trackList = [NSArray new];
			if (array != nil) {
				trackList = [self trackItems:array];
			}
			
			successHandler(trackList);
		} failure:failureHandler];
	});
}

/// Initialize Track item,
/// from one dimensonal json data to two dimenson
- (NSArray *)trackItems:(NSArray *)items {
	NSMutableArray *movies = [NSMutableArray new];
	NSMutableArray *musics = [NSMutableArray new];
	NSMutableArray *results = [NSMutableArray new];
	
	for (int i = 0; i < items.count; i++) {
		NSDictionary *dict = items[i];
		Track *track = [self transformTrackFromDictionary:dict];
		
		// Kind
		NSString *kindItem = dict[@"kind"];
		if ([kindItem isEqualToString:@"song"]) {
			[musics addObject:track];
		} else if ([kindItem isEqualToString:@"feature-movie"]) {
			[movies addObject:track];
		}
	}
	
	if (movies.count > 0) {
		TrackList *movieList = [[TrackList alloc] initWithKind:movie tracks:movies];
		[results addObject:movieList];
	}
	
	if (musics.count > 0) {
		TrackList *musicList = [[TrackList alloc] initWithKind:music tracks:musics];
		[results addObject:musicList];
	}
	
	return results;
}


/// NSDictionary -> Track
- (Track *)transformTrackFromDictionary:(NSDictionary *)dictionary {
	// CollectionId
	NSString *trackId = [NSString stringWithFormat:@"%@", dictionary[@"trackId"]];
	
	// Thumb
	NSString *thumbURLString = [self checkStringNull:dictionary[@"artworkUrl60"]];
	
	// Track
	NSString *trackName = [self checkStringNull:dictionary[@"trackName"]];
	
	// Artist
	NSString *artist = [self checkStringNull:dictionary[@"artistName"]];
	
	// Collection
	NSString *collection = [self checkStringNull:dictionary[@"collectionCensoredName"]];
	
	// Length
	NSString *length = [NSString stringWithFormat:@"%@", dictionary[@"trackTimeMillis"]];
	
	// Detail
	NSString *detail = dictionary[@"longDescription"];
	
	// TrackViewURL
	NSString *trackViewURLString = dictionary[@"trackViewUrl"];
	
	Track *track = [[Track alloc] initWithTrackId:trackId thumbURL:thumbURLString track:trackName artist:artist collection:collection length:length detail:detail trackViewURLString:trackViewURLString];
	
	return track;
}

- (NSString *)checkStringNull:(id)string {
	if ([string isKindOfClass:[NSString class]]) {
		return (NSString *)string;
	}
	
	return @"";
}


#pragma mark - OpenURL

/// Open URL
- (void)openURL:(NSURL *)url {
	UIApplication *application = [UIApplication sharedApplication];
	if ([application canOpenURL:url]) {
		[application openURL:url options:@{} completionHandler:nil];
	}
}


@end
