//
//  DataManager.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Track.h"

NS_ASSUME_NONNULL_BEGIN

@interface DataManager : NSObject

+ (instancetype)sharedInstance;

- (void)getDataWithKeyword:(NSString *)keyword successHandler:(void (^)(NSArray * trackList))successHandler failureHandler:(void (^)(NSError *error))failureHandler;

- (Track *)transformTrackFromDictionary:(NSDictionary *)dictionary;

- (void)openURL:(NSURL *)url;

@end

NS_ASSUME_NONNULL_END
