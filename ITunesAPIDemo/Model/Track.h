//
//  Track.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Track : NSObject

@property (strong, nonatomic) NSString *trackId;
@property (strong, nonatomic) NSString *thumbURLString;
@property (strong, nonatomic) NSString *track;
@property (strong, nonatomic) NSString *artist;
@property (strong, nonatomic) NSString *collection;
@property (strong, nonatomic) NSString *length;
@property (strong, nonatomic) NSString *detail;
@property (strong, nonatomic) NSString *trackViewURLString;


// Common init method
- (id)initWithTrackId:(NSString *)trackId thumbURL:(NSString *)thumbURL track:(NSString *)track artist:(NSString *)artist collection:(NSString *)collection length:(NSString *)length detail:(NSString * _Nullable)detail trackViewURLString:(NSString *)trackViewURLString;

// For save
- (NSDictionary *)transformToDictionary;

@end

NS_ASSUME_NONNULL_END
