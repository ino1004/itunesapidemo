//
//  TrackList.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/25.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "TrackList.h"

@implementation TrackList

- (id)initWithKind:(TrackKind)kind tracks:(NSArray *)tracks {
	self = [super init];
	if (self) {
		self.kind = kind;
		self.tracks = tracks;
	}
	
	return self;
}

@end
