//
//  TrackList.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/25.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Track.h"

NS_ASSUME_NONNULL_BEGIN

@interface TrackList : NSObject

typedef NS_ENUM(NSUInteger, TrackKind) {
	movie = 0,
	music = 1,
	undefined = 2
};

@property (nonatomic) TrackKind kind;
@property (strong, nonatomic) NSArray<Track *> *tracks;


- (id)initWithKind:(TrackKind)kind tracks:(NSArray *)tracks;

@end

NS_ASSUME_NONNULL_END
