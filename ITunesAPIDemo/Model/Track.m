//
//  Track.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "Track.h"

@implementation Track

- (id)initWithTrackId:(NSString *)trackId thumbURL:(NSString *)thumbURL track:(NSString *)track artist:(NSString *)artist collection:(NSString *)collection length:(NSString *)length detail:(NSString * _Nullable)detail trackViewURLString:(NSString *)trackViewURLString {
	self = [super init];
	if (self) {
		self.trackId = trackId;
		self.thumbURLString = thumbURL;
		self.track = track;
		self.artist = artist;
		self.collection = collection;
		self.length = length;
		self.detail = detail;
		self.trackViewURLString = trackViewURLString;
	}
	
	return self;
}

/// Track -> NSDictionary
- (NSDictionary *)transformToDictionary {
	NSMutableDictionary *dict = [NSMutableDictionary new];
	[dict setValue:self.trackId forKey:@"trackId"];
	[dict setValue:self.thumbURLString forKey:@"artworkUrl60"];
	[dict setValue:self.track forKey:@"trackName"];
	[dict setValue:self.artist forKey:@"artistName"];
	[dict setValue:self.collection forKey:@"collectionCensoredName"];
	[dict setValue:self.length forKey:@"trackTimeMillis"];
	if (self.detail != nil) {
		[dict setValue:self.detail forKey:@"longDescription"];
	}
	[dict setValue:self.trackViewURLString forKey:@"trackViewUrl"];
	
	return dict;
}

@end
