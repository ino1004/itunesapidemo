//
//  UIColor+Hex.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (UIColor*)colorWithHexString:(NSString*)hex;

@end
