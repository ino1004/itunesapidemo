//
//  FavoritePageCell.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/27.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "FavoritePageCell.h"
#import "TrackCell.h"
#import "FavoriteManager.h"
#import "DataManager.h"

@interface FavoritePageCell() <UITableViewDelegate, UITableViewDataSource, TrackCellDelegate>

@property (strong, nonatomic) NSMutableArray<NSIndexPath *> *expandedIndexPaths;

@end

@implementation FavoritePageCell

- (void)awakeFromNib {
	[super awakeFromNib];
	
	self.expandedIndexPaths = [NSMutableArray new];
	
	// Register cell
	[self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TrackCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([TrackCell class])];
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
}


#pragma mark - Binding

- (void)bindingWithKind:(TrackKind)kind {
	self.kind = kind;
	[self.tableView reloadData];
}


#pragma mark - Action

- (NSArray<Track *> *)updatedTracks {
	return [[FavoriteManager sharedInstance] loadTracksWithKind:self.kind];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self updatedTracks].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	TrackCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TrackCell class]) forIndexPath:indexPath];
	cell.delegate = self;
	[cell bindingWithTrack:[self updatedTracks][indexPath.row] kind:self.kind];
	[cell checkExpandedIndexPath:indexPath inIndexPaths:self.expandedIndexPaths kind:self.kind];
	return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	Track *track = [self updatedTracks][indexPath.row];
	
	NSURL *url = [NSURL URLWithString:track.trackViewURLString];
	[[DataManager sharedInstance] openURL:url];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewAutomaticDimension;
}


#pragma mark - TrackCellDelegate

- (void)trackCell:(TrackCell *)trackCell favoriteButtonDidPress:(UIButton *)button {
	NSIndexPath *indexPath = [self.tableView indexPathForCell:trackCell];
	Track *track = [self updatedTracks][indexPath.row];
	
	BOOL isFavorited = [[FavoriteManager sharedInstance] isFavoritedWithTrack:track kind:self.kind];
	[trackCell setFavorite:isFavorited];
	[self.tableView reloadData];
}

- (void)trackCell:(TrackCell *)trackCell readMoreButtonDidPress:(UIButton *)button {
	NSIndexPath *indexPath = [self.tableView indexPathForCell:trackCell];
	[self.expandedIndexPaths addObject:indexPath];
	[self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

@end
