//
//  FavoritePageCell.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/27.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Track.h"
#import "TrackList.h"

NS_ASSUME_NONNULL_BEGIN

@interface FavoritePageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) TrackKind kind;

- (void)bindingWithKind:(TrackKind)kind;

@end

NS_ASSUME_NONNULL_END
