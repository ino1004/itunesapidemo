//
//  TrackCell.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "TrackCell.h"
#import "ThemeColor.h"
#import "FavoriteManager.h"
#import <SDWebImage/UIImageView+WebCache.h>


@implementation TrackCell

static NSString *favoriteStr = @"  收藏  ";
static NSString *unFavoriteStr = @"  取消收藏  ";

- (void)awakeFromNib {
    [super awakeFromNib];
	[self setupViews];
}

- (void)prepareForReuse {
	[super prepareForReuse];
	[self.favoriteButton setTitle:favoriteStr forState:UIControlStateNormal];
	[self.stackViewBottomConstraint setPriority:UILayoutPriorityDefaultHigh];
	[self.stackViewBottomConstraintForMusic setPriority:UILayoutPriorityDefaultLow];
}


#pragma mark - Settings

- (void)setupViews {
	[self.thumbImageView.layer setCornerRadius:6];
	[self.favoriteButton setTitle:favoriteStr forState:UIControlStateNormal];
	[[self.favoriteButton layer] setCornerRadius:6];
}


#pragma mark - Binding

- (void)bindingWithTrack:(Track *)track kind:(TrackKind)kind {
	[self.favoriteButton setBackgroundColor:[ThemeColor sharedInstance].currentThemeColor];
	[self.trackLabel setText:track.track];
	[self.artistLabel setText:track.artist];
	[self.collectionLabel setText:track.collection];
	
	// Length to minute
	float millis = [track.length floatValue];
	float seconds = millis / 1000.0;
	float minutes = seconds / 60;
	int second = (int)seconds % 60;
	[self.lengthLabel setText:[NSString stringWithFormat:@"%.0f:%.2d", minutes, second]];
	
	if (track.detail != nil) {
		[self.descriptionLabel setHidden:false];
		[self.descriptionLabel setText:track.detail];
		[self.readMoreButton setHidden:false];
	}
	[self.thumbImageView sd_setImageWithURL:[NSURL URLWithString:track.thumbURLString]];

	// Layout
	switch (kind) {
		case movie:
			[self.stackViewBottomConstraint setPriority:UILayoutPriorityDefaultHigh];
			[self.stackViewBottomConstraintForMusic setPriority:UILayoutPriorityDefaultLow];
			break;
		case music:
			[self.descriptionLabel setHidden:true];
			[self.readMoreButton setHidden:true];
			
			[self.stackViewBottomConstraint setPriority:UILayoutPriorityDefaultLow];
			[self.stackViewBottomConstraintForMusic setPriority:UILayoutPriorityDefaultHigh];
		default:
			break;
	}
	
	[self layoutIfNeeded];
	
	// Check the favoriteManager contains track or not
	[self.favoriteButton setTitle:favoriteStr forState:UIControlStateNormal];
	NSArray<Track *> *tracks = [[FavoriteManager sharedInstance] loadTracksWithKind:kind];
	for (int i = 0; i < tracks.count; i++) {
		if ([tracks[i].trackId isEqualToString:track.trackId]) {
			[self.favoriteButton setTitle:unFavoriteStr forState:UIControlStateNormal];
		}
	}
}


#pragma mark - Action

/// Check the cell is expanded or not
- (void)checkExpandedIndexPath:(NSIndexPath *)indexPath inIndexPaths:(NSArray *)indexPaths kind:(TrackKind)kind {
	for (NSIndexPath *expandedIndexPath in indexPaths) {
		if (indexPath == expandedIndexPath) {
			// Expanded
			[self setExpanded];
			return;
		}
	}
	
	// Collapsed
	if (kind == movie) {
		[self setCollapsed];
	}
}

- (void)setExpanded {
	[self.readMoreButton setHidden:true];
	[self.descriptionLabel setNumberOfLines:0];
	[self.descriptionLabel setLineBreakMode:NSLineBreakByWordWrapping];
	[self.readMoreButtonHeightConstraint setConstant:0];
	[self layoutIfNeeded];
}

- (void)setCollapsed {
	[self.readMoreButton setHidden:false];
	[self.descriptionLabel setNumberOfLines:2];
	[self.readMoreButtonHeightConstraint setConstant:20];
	[self layoutIfNeeded];
}

- (void)setFavorite:(BOOL)isFavorite {
	if (isFavorite) {
		[self.favoriteButton setTitle:favoriteStr forState:UIControlStateNormal];
	} else {
		[self.favoriteButton setTitle:unFavoriteStr forState:UIControlStateNormal];
	}
}


# pragma mark - Delegate

- (IBAction)favoriteButtonDidPress:(UIButton *)sender {
	[self.delegate trackCell:self favoriteButtonDidPress:sender];
}

- (IBAction)readMoreButtonDidPress:(UIButton *)sender {
	[self setExpanded];
	[self.delegate trackCell:self readMoreButtonDidPress:sender];
}

@end
