//
//  PersonalCell.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/27.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThemeColor.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersonalCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

- (void)bindingWithTitle:(NSString *)title theme:(Theme *)theme atIndexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
