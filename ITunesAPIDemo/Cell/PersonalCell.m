//
//  PersonalCell.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/27.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "PersonalCell.h"
#import "FavoriteManager.h"

@implementation PersonalCell

- (void)awakeFromNib {
    [super awakeFromNib];
}


#pragma mark - Binding

- (void)bindingWithTitle:(NSString *)title theme:(Theme *)theme atIndexPath:(NSIndexPath *)indexPath {
	// Title label
	[self.titleLabel setText:title];
	
	// Detail label
	NSString *detail = @"";
	if (indexPath.section == 0) {
		detail = theme.colorTitle;
	} else {
		int movieCount = (int)[[FavoriteManager sharedInstance] loadTracksWithKind:movie].count;
		int musicCount = (int)[[FavoriteManager sharedInstance] loadTracksWithKind:music].count;
		detail = [NSString stringWithFormat:@"共有 %d 項收藏", movieCount + musicCount];
	}

	[self.detailLabel setText:detail];
}

@end
