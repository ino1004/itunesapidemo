//
//  TrackCell.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Track.h"
#import "TrackList.h"

NS_ASSUME_NONNULL_BEGIN

@class TrackCell;
@protocol TrackCellDelegate <NSObject>

- (void)trackCell:(TrackCell *)trackCell favoriteButtonDidPress:(UIButton *)button;
@optional
- (void)trackCell:(TrackCell *)trackCell readMoreButtonDidPress:(UIButton *)button;

@end

@interface TrackCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (weak, nonatomic) IBOutlet UILabel *trackLabel;
@property (weak, nonatomic) IBOutlet UILabel *artistLabel;
@property (weak, nonatomic) IBOutlet UILabel *collectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *lengthLabel;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *readMoreButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewBottomConstraintForMusic;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionLabelBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionLabelBottomConstraintForMovieExpand;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *readMoreButtonHeightConstraint;


@property (weak, nonatomic) id <TrackCellDelegate> delegate;

- (void)bindingWithTrack:(Track *)track kind:(TrackKind)kind;

- (void)checkExpandedIndexPath:(NSIndexPath *)indexPath inIndexPaths:(NSArray *)indexPaths kind:(TrackKind)kind;

- (void)setFavorite:(BOOL)isFavorite;

@end

NS_ASSUME_NONNULL_END
