//
//  ThemeColor.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Theme.h"

NS_ASSUME_NONNULL_BEGIN

@interface ThemeColor : NSObject

//typedef NS_ENUM(NSUInteger, Themeee) {
//	dark = 0,
//	light = 1
//};

+ (instancetype)sharedInstance;

- (Theme *)currentTheme;

- (void)saveThemeType:(ThemeType)themeType;
- (ThemeType)currentThemeType;
- (UIColor *)currentThemeColor;

@end

NS_ASSUME_NONNULL_END
