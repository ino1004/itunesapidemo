//
//  Theme.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/27.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Theme : NSObject

typedef NS_ENUM(NSUInteger, ThemeType) {
	dark = 0,
	light = 1
};

@property (strong, nonatomic) UIColor *color;
@property (nonatomic) ThemeType themeType;
@property (strong, nonatomic) NSString *colorTitle;

- (id)initWithColor:(UIColor *)color themeType:(ThemeType)themeType colorTitle:(NSString *)colorTitle;

@end

NS_ASSUME_NONNULL_END
