//
//  FavoriteManager.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "FavoriteManager.h"
#import "DataManager.h"

@implementation FavoriteManager

static NSString *movieKey = @"movie";
static NSString *musicKey = @"music";

+ (instancetype)sharedInstance {
	static FavoriteManager *favoriteManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		favoriteManager = [[FavoriteManager alloc] init];
	});
	return favoriteManager;
}

/// Check favorite state
- (BOOL)isFavoritedWithTrack:(Track *)track kind:(TrackKind)kind {
	NSArray<Track *> *tracks = [self loadTracksWithKind:kind];
	for (Track *savedTrack in tracks) {
		if ([savedTrack.trackId isEqualToString:track.trackId]) {
			[self deleteTrack:savedTrack kind:kind];
			return true;
		}
	}
	
	[self saveTrack:track kind:kind];
	return false;
}

/// Save track
- (void)saveTrack:(Track *)track kind:(TrackKind)kind {
	NSMutableArray *results = [NSMutableArray new];
	
	// Transform track to dictionary first
	NSDictionary *targetTrackDict = [track transformToDictionary];
	
	// Get the saved tracks and append on it
	NSMutableArray *tracks = [NSMutableArray new];
	tracks = [[self loadTracksWithKind:kind] mutableCopy];
	
	// Return if exists
	for (Track *savedTrack in tracks) {
		if (savedTrack.trackId == track.trackId) {
			return;
		}
	}

	// Append to results
	for (int i = 0; i < tracks.count; i++) {
		NSDictionary *dict = [tracks[i] transformToDictionary];
		[results addObject:dict];
	}
	[results addObject:targetTrackDict];
	
	// Save
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	switch (kind) {
		case movie:
			[userDefaults setObject:results forKey:movieKey];
			break;
		case music:
			[userDefaults setObject:results forKey:musicKey];
			break;
		default:
			break;
	}
	
	[userDefaults synchronize];
}

/// Load tracks
- (NSArray<Track *> *)loadTracksWithKind:(TrackKind)kind {
	NSMutableArray<Track *> *results = [NSMutableArray new];
	NSArray<NSDictionary *> *tracks = [NSArray new];
	
	// Get the saved tracks
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	switch (kind) {
		case movie:
			tracks = [userDefaults objectForKey:movieKey];
			break;
		case music:
			tracks = [userDefaults objectForKey:musicKey];
			break;
		default:
			break;
	}
	
	// Transform dictionary to track
	for (int i = 0; i < tracks.count; i++) {
		NSDictionary *dict = tracks[i];
		Track *track = [[DataManager sharedInstance] transformTrackFromDictionary:dict];
		[results addObject:track];
	}
	
	return results;
}


/// Delete
- (void)deleteTrack:(Track *)track kind:(TrackKind)kind {
	NSMutableArray<Track *> *tracks = [[self loadTracksWithKind:kind] mutableCopy];
	// Remove the same track from the tracks array
	int isSameTrackIndex = -1;
	for (int i = 0; i < tracks.count; i++) {
		if ([tracks[i].trackId isEqualToString:track.trackId]) {
			isSameTrackIndex = i;
		}
	}
	if (isSameTrackIndex >= 0) {
		[tracks removeObjectAtIndex:isSameTrackIndex];
	}

	// Save to userDefaults
	NSMutableArray *results = [NSMutableArray new];
	for (Track *track in tracks) {
		NSDictionary *trackDict = [track transformToDictionary];
		[results addObject:trackDict];
	}
	
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	switch (kind) {
		case movie:
			[userDefaults setObject:results forKey:movieKey];
			break;
		case music:
			[userDefaults setObject:results forKey:musicKey];
			break;
		default:
			break;
	}
	
	[userDefaults synchronize];
}

@end
