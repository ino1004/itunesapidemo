//
//  Theme.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/27.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "Theme.h"

@implementation Theme

- (id)initWithColor:(UIColor *)color themeType:(ThemeType)themeType colorTitle:(NSString *)colorTitle {
	self = [super init];
	if (self) {
		self.color = color;
		self.themeType = themeType;
		self.colorTitle = colorTitle;
	}
	
	return self;
}

@end
