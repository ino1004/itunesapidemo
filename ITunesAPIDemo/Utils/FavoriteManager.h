//
//  FavoriteManager.h
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Track.h"
#import "TrackList.h"

NS_ASSUME_NONNULL_BEGIN

@interface FavoriteManager : NSObject

+ (instancetype)sharedInstance;

- (BOOL)isFavoritedWithTrack:(Track *)track kind:(TrackKind)kind;

- (void)saveTrack:(Track *)track kind:(TrackKind)kind;

- (NSArray<Track *> *)loadTracksWithKind:(TrackKind)kind;

@end

NS_ASSUME_NONNULL_END
