//
//  ThemeColor.m
//  ITunesAPIDemo
//
//  Created by Stephen Chui on 2019/6/24.
//  Copyright © 2019 Stephen Chui. All rights reserved.
//

#import "ThemeColor.h"
#import "UIColor+Hex.h"


@implementation ThemeColor

+ (instancetype)sharedInstance {
	static ThemeColor *themeColor = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		themeColor = [[ThemeColor alloc] init];
	});
	return themeColor;
}

- (UIColor *)darkTheme {
	return [UIColor colorWithHexString:@"00796B"];
}

- (UIColor *)lightTheme {
	return [UIColor colorWithHexString:@"4DB6AC"];
}


/// Save theme
- (void)saveThemeType:(ThemeType)themeType {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSString *hex = @"";
	switch (themeType) {
		case dark:
			hex = [self hexStringFromColor:[self darkTheme]];
			break;
		case light:
			hex = [self hexStringFromColor:[self lightTheme]];
			break;
		default:
			break;
	}
	[userDefaults setObject:hex forKey:@"Theme"];
	[userDefaults synchronize];
}

/// Get the current theme
- (Theme *)currentTheme {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSString *hex = [userDefaults objectForKey:@"Theme"];
	
	Theme *darkTheme = [[Theme alloc] initWithColor:[self darkTheme] themeType:dark colorTitle:@"深色主題"];
	Theme *lightTheme = [[Theme alloc] initWithColor:[self lightTheme] themeType:light colorTitle:@"淺色主題"];
	
	if (hex == nil) {
		return darkTheme;
	}
	
	if ([hex isEqualToString:[self hexStringFromColor:[self darkTheme]]]) {
		return darkTheme;
	}
	
	return lightTheme;
}

/// Get the `ThemeType`
- (ThemeType)currentThemeType {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSString *hex = [userDefaults objectForKey:@"Theme"];
	// Return dark if userDefaults not exists
	if (hex == nil) {
		return dark;
	}
	
	if ([hex isEqualToString:[self hexStringFromColor:[self darkTheme]]]) {
		return dark;
	} else {
		return light;
	}
}

/// Get the themeColor `UIColor`
- (UIColor *)currentThemeColor {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSString *hex = [userDefaults objectForKey:@"Theme"];
	// Return darkTheme if userDefaults not exists
	if (hex == nil) {
		return [self darkTheme];
	}
	
	return [self colorWithHexString:hex];
}


#pragma mark - Color conversion

- (NSString *)hexStringFromColor:(UIColor *)color {
	const CGFloat *components = CGColorGetComponents(color.CGColor);
	CGFloat r = components[0];
	CGFloat g = components[1];
	CGFloat b = components[2];
	NSString *hexString=[NSString stringWithFormat:@"%02X%02X%02X", (int)(r * 255), (int)(g * 255), (int)(b * 255)];
	
	return hexString;
}

- (UIColor *)colorWithHexString:(NSString *)hexString {
	NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
	CGFloat alpha, red, blue, green;
	
	// #RGB
	alpha = 1.0f;
	red   = [self colorComponentFrom: colorString start: 0 length: 2];
	green = [self colorComponentFrom: colorString start: 2 length: 2];
	blue  = [self colorComponentFrom: colorString start: 4 length: 2];
	
	return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat)colorComponentFrom:(NSString *)string start:(NSUInteger)start length: (NSUInteger) length {
	NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
	NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
	unsigned hexComponent;
	[[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
	return hexComponent / 255.0;
}

@end
